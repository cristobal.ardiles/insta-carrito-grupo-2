from django.contrib import admin

from .models import (
    Client,
    Shopper,
    Route,
    Order,
    Store,
    DeliveryAddress,
    Product,
    BackgroundCheck,
    ClientPayment,
    ShopperPayment,
)

# Register your models here.

# from .models import IntegerBox

# Register your models here.

# --- exmaples ---

# admin.site.register(IntegerBox)

# --- project ---

admin.site.register(Client)
admin.site.register(Shopper)
admin.site.register(Route)
admin.site.register(Order)
admin.site.register(Store)
admin.site.register(DeliveryAddress)
admin.site.register(Product)
admin.site.register(BackgroundCheck)
admin.site.register(ClientPayment)
admin.site.register(ShopperPayment)
