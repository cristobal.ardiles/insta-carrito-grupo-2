from rest_framework import serializers
from rest_framework.serializers import SerializerMethodField
import math

from .models import (
    Client,
    Shopper,
    Route,
    Order,
    Store,
    Product,
    DeliveryAddress,
    BackgroundCheck,
    ClientPayment,
    ShopperPayment,
)


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = [
            "first_name",
            "last_name",
            "email",
            "associated_card_number",
            "associated_card_type",
            "score",
        ]


class ShopperSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shopper
        fields = [
            "first_name",
            "last_name",
            "rut",
            "bank",
            "bank_account_number",
            "email",
            "score",
        ]


class DeliveryAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeliveryAddress
        fields = [
            "client",
            "name",
            "tag",
            "address",
            "lat_coord",
            "lon_coord",
            "misc",
        ]


class StoreSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Store
        fields = [
            "id",
            "name",
            "category",
            "address",
            "lat_coord",
            "lon_coord",
        ]

    def create(self, validated_data):
        validated_data.pop("id")
        obj = Store.objects.create(**validated_data)
        obj.save()
        return obj


class ProductSerializer(serializers.ModelSerializer):
    stores = StoreSerializer(many=True)
    id = serializers.IntegerField()

    class Meta:
        model = Product
        fields = [
            "id",
            "name",
            "price",
            "category",
            "brand",
            "stores",
        ]

    def create(self, validated_data):
        validated_data.pop("id")
        stores = validated_data.pop("stores")
        obj = Product.objects.create(**validated_data)
        for store in stores:
            obj.stores.add(store["id"])
        obj.save()
        return obj


class OrderSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)
    order_routes = SerializerMethodField()

    class Meta:
        model = Order
        fields = [
            "client",
            "shopper",
            "delivery_address",
            "created_at",
            "deliver_at",
            "order_routes",
            "price",
            "payment",
            "completion_state",
            "products",
        ]

    def get_order_routes(self, order):
        order_routes = Route.objects.filter(order=order.id)
        serializer = RouteSerializer(order_routes, many=True)
        return serializer.data

    def create(self, validated_data):
        products = validated_data.pop("products")
        obj = Order.objects.create(**validated_data)
        for product in products:
            obj.products.add(product["id"])
        obj.save()
        return obj


class RouteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Route
        fields = ["lat_coord", "lon_coord", "timestamp", "order"]


class BackgroundCheckSerializer(serializers.ModelSerializer):
    class Meta:
        model = BackgroundCheck
        fields = [
            "shopper",
            "age",
            "criminal_record",
            "profile_headshot",
            "id_photo",
            "status_of_procedure",
        ]


class ClientPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientPayment
        fields = [
            "client",
            "created_at",
            "order",
            "amount",
            "payment_method",
            "status",
        ]

    def create(self, validated_data):
        obj = ClientPayment.objects.create(**validated_data)
        order = validated_data["order"]
        obj.amount = math.ceil(order.price * 1.1)
        obj.payment_method = order.payment
        obj.save()
        return obj


class ShopperPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShopperPayment
        fields = [
            "shopper",
            "created_at",
            "order",
            "amount",
            "bank",
            "bank_account_number",
            "status",
        ]

    def create(self, validated_data):
        obj = ShopperPayment.objects.create(**validated_data)
        order = validated_data["order"]
        shopper = validated_data["shopper"]
        obj.amount = math.ceil(order.price * 1.1)
        obj.bank = shopper.bank
        obj.bank_account_number = shopper.bank_account_number
        obj.save()
        return obj
