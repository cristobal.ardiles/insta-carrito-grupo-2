import io
from model_bakery import baker
import pytest
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer

from app.models import (
    Client,
    Shopper,
    Route,
    Order,
    Store,
    Product,
    DeliveryAddress,
    BackgroundCheck,
    ClientPayment,
    ShopperPayment,
)
from app.serializers import (
    ClientSerializer,
    ShopperSerializer,
    OrderSerializer,
    StoreSerializer,
    ProductSerializer,
    DeliveryAddressSerializer,
    RouteSerializer,
    BackgroundCheckSerializer,
    ClientPaymentSerializer,
    ShopperPaymentSerializer,
)

# --- project ---

# fixtures to be seen for all test classes


@pytest.fixture
def client_and_serializer():
    client = baker.make_recipe("app.tests.example_client")

    expected_client_serializer = {
        "first_name": client.first_name,
        "last_name": client.last_name,
        "email": client.email,
        "associated_card_number": client.associated_card_number,
        "associated_card_type": client.associated_card_type,
        "score": client.score,
    }

    return [client, expected_client_serializer]


@pytest.fixture
def shopper_and_serializer():
    shopper = baker.make_recipe("app.tests.example_shopper")

    expected_shopper_serializer = {
        "first_name": shopper.first_name,
        "last_name": shopper.last_name,
        "email": shopper.email,
        "rut": shopper.rut,
        "bank": shopper.bank,
        "bank_account_number": shopper.bank_account_number,
        "score": shopper.score,
    }

    return [shopper, expected_shopper_serializer]


@pytest.fixture
def store_and_serializer():
    store = baker.make_recipe("app.tests.example_store")

    expected_store_serializer = {
        "id": store.id,
        "name": store.name,
        "category": store.category,
        "address": store.address,
        "lat_coord": store.lat_coord,
        "lon_coord": store.lon_coord,
    }

    return [store, expected_store_serializer]


@pytest.fixture
def delivery_address_and_serializer():
    delivery_address = baker.make_recipe("app.tests.example_delivery_address")

    expected_delivery_address_serializer = {
        "client": delivery_address.client_id,
        "name": delivery_address.name,
        "tag": delivery_address.tag,
        "address": delivery_address.address,
        "lat_coord": delivery_address.lat_coord,
        "lon_coord": delivery_address.lon_coord,
        "misc": delivery_address.misc,
    }

    return [delivery_address, expected_delivery_address_serializer]


@pytest.fixture
def product_and_serializer():
    product = baker.make_recipe("app.tests.example_product1")

    expected_product_serializer = {
        "id": product.id,
        "name": product.name,
        "price": product.price,
        "category": product.category,
        "brand": product.brand,
        "stores": StoreSerializer(product.stores.all(), many=True).data,
    }

    return [product, expected_product_serializer]


@pytest.fixture
def order_and_serializer():
    order = baker.make_recipe("app.tests.example_order")

    expected_order_serializer = {
        "client": order.client_id,
        "shopper": order.shopper_id,
        "delivery_address": order.delivery_address_id,
        "created_at": order.created_at.isoformat(),
        "deliver_at": order.deliver_at.isoformat(),
        "payment": order.payment,
        "completion_state": order.completion_state,
        "products": ProductSerializer(order.products.all(), many=True).data,
        "order_routes": [],
        "price": order.price,
    }

    return [order, expected_order_serializer]


@pytest.fixture
def route_and_serializer():
    route = baker.make_recipe("app.tests.example_route")

    expected_route_serializer = {
        "lat_coord": route.lat_coord,
        "lon_coord": route.lon_coord,
        "timestamp": route.timestamp.isoformat(),
        "order": route.order_id,
    }

    return [route, expected_route_serializer]


@pytest.fixture
def background_check_and_serializer():
    background_check = baker.make_recipe(
        "app.tests.example_background_check",
        _fill_optional=True,
        _create_files=True,
    )

    expected_background_check_serializer = {
        "shopper": background_check.shopper_id,
        "age": background_check.age,
        "criminal_record": background_check.criminal_record.url,
        "profile_headshot": background_check.profile_headshot.url,
        "id_photo": background_check.id_photo.url,
        "status_of_procedure": background_check.status_of_procedure,
    }

    return [background_check, expected_background_check_serializer]


@pytest.fixture
def client_payment_and_serializer():
    client_payment = baker.make_recipe("app.tests.example_client_payment")

    expected_client_payment_serializer = {
        "client": client_payment.client_id,
        "created_at": client_payment.created_at.isoformat(),
        "order": client_payment.order_id,
        "amount": client_payment.amount,
        "payment_method": client_payment.payment_method,
        "status": client_payment.status,
    }

    return [client_payment, expected_client_payment_serializer]


@pytest.fixture
def shopper_payment_and_serializer():
    shopper_payment = baker.make_recipe("app.tests.example_shopper_payment")

    expected_shopper_payment_serializer = {
        "shopper": shopper_payment.shopper_id,
        "created_at": shopper_payment.created_at.isoformat(),
        "order": shopper_payment.order_id,
        "amount": shopper_payment.amount,
        "bank": shopper_payment.bank,
        "bank_account_number": shopper_payment.bank_account_number,
        "status": shopper_payment.status,
    }

    return [shopper_payment, expected_shopper_payment_serializer]


# test classes


class TestClientSerializer:
    @pytest.mark.django_db
    def test_client_serialization(self, client_and_serializer):
        client = client_and_serializer[0]
        expected_serializer_data = client_and_serializer[1]
        serializer_data = ClientSerializer(client).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_client_deserialization(self, client_and_serializer):
        client = client_and_serializer[0]
        content = JSONRenderer().render(client_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = ClientSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        clients = Client.objects.all()

        assert clients.count() == 2
        assert clients.last().first_name == client.first_name
        assert clients.last().last_name == client.last_name


class TestShopperSerializer:
    @pytest.mark.django_db
    def test_shopper_serialization(self, shopper_and_serializer):
        shopper = shopper_and_serializer[0]
        expected_serializer_data = shopper_and_serializer[1]
        serializer_data = ShopperSerializer(shopper).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_shopper_deserialization(self, shopper_and_serializer):
        shopper = shopper_and_serializer[0]
        content = JSONRenderer().render(shopper_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = ShopperSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        shoppers = Shopper.objects.all()

        assert shoppers.count() == 2
        assert shoppers.last().first_name == shopper.first_name
        assert shoppers.last().last_name == shopper.last_name


class TestRouteSerializer:
    @pytest.mark.django_db
    def test_route_serialization(self, route_and_serializer):
        route = route_and_serializer[0]
        expected_serializer_data = route_and_serializer[1]
        serializer_data = RouteSerializer(route).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_route_deserialization(
        self,
        route_and_serializer,
    ):
        route = route_and_serializer[0]
        expected_serializer = route_and_serializer[1]
        content = JSONRenderer().render(expected_serializer)
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = RouteSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        routes = Route.objects.all()

        assert routes.count() == 2
        assert routes.last().lat_coord == route.lat_coord
        assert routes.last().lon_coord == route.lon_coord


class TestStoreSerializer:
    @pytest.mark.django_db
    def test_store_serialization(self, store_and_serializer):
        store = store_and_serializer[0]
        expected_serializer_data = store_and_serializer[1]
        serializer_data = StoreSerializer(store).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_store_deserialization(self, store_and_serializer):
        store = store_and_serializer[0]
        content = JSONRenderer().render(store_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = StoreSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        stores = Store.objects.all()

        assert stores.count() == 2
        assert stores.last().name == store.name
        assert stores.last().category == store.category
        assert stores.last().lat_coord == store.lat_coord


class TestDeliveryAddressSerializer:
    @pytest.mark.django_db
    def test_delivery_address_serialization(self, delivery_address_and_serializer):
        delivery_address = delivery_address_and_serializer[0]
        expected_serializer_data = delivery_address_and_serializer[1]
        serializer_data = DeliveryAddressSerializer(delivery_address).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_delivery_address_deserialization(
        self, delivery_address_and_serializer
    ):
        delivery_address = delivery_address_and_serializer[0]
        content = JSONRenderer().render(delivery_address_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = DeliveryAddressSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        delivery_addresses = DeliveryAddress.objects.all()

        assert delivery_addresses.count() == 2
        assert delivery_addresses.last().name == delivery_address.name
        assert delivery_addresses.last().tag == delivery_address.tag
        assert delivery_addresses.last().lat_coord == delivery_address.lat_coord


class TestProductSerializer:
    @pytest.mark.django_db
    def test_product_serialization(self, product_and_serializer):
        product = product_and_serializer[0]
        expected_serializer_data = product_and_serializer[1]
        serializer_data = ProductSerializer(product).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_product_deserialization(self, product_and_serializer):
        product = product_and_serializer[0]
        content = JSONRenderer().render(product_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = ProductSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        products = Product.objects.all()

        assert products.count() == 2
        assert products.last().name == product.name
        assert products.last().brand == product.brand
        assert (
            products.last().stores.all().first().name
            == product.stores.all().first().name
        )
        assert products.last().stores.all().count() == product.stores.all().count()


class TestOrderSerializer:
    @pytest.mark.django_db
    def test_order_serialization(self, order_and_serializer):
        order = order_and_serializer[0]
        expected_serializer_data = order_and_serializer[1]
        serializer_data = OrderSerializer(order).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_order_deserialization(self, order_and_serializer):
        order = order_and_serializer[0]
        content = JSONRenderer().render(order_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = OrderSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        orders = Order.objects.all()

        assert orders.count() == 2
        assert orders.last().price == order.price
        assert orders.last().client_id == order.client_id
        assert (
            orders.last().products.all().first().name
            == order.products.all().first().name
        )
        assert orders.last().products.all().count() == order.products.all().count()


class TestBackgroundCheckSerializer:
    @pytest.mark.django_db
    def test_background_check_serialization(self, background_check_and_serializer):
        background_check = background_check_and_serializer[0]
        expected_serializer_data = background_check_and_serializer[1]
        serializer_data = BackgroundCheckSerializer(background_check).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_background_check_deserialization(
        self, background_check_and_serializer
    ):
        background_check = background_check_and_serializer[0]
        content = JSONRenderer().render(background_check_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        data["criminal_record"] = background_check.criminal_record
        data["profile_headshot"] = background_check.profile_headshot
        data["id_photo"] = background_check.id_photo
        serializer = BackgroundCheckSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        background_checks = BackgroundCheck.objects.all()

        assert background_checks.count() == 2
        assert background_checks.last().shopper_id == background_check.shopper_id
        assert background_checks.last().id_photo == background_check.id_photo


class TestClientPaymentSerializer:
    @pytest.mark.django_db
    def test_client_payment_serialization(self, client_payment_and_serializer):
        client_payment = client_payment_and_serializer[0]
        expected_serializer_data = client_payment_and_serializer[1]
        serializer_data = ClientPaymentSerializer(client_payment).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_client_payment_deserialization(self, client_payment_and_serializer):
        client_payment = client_payment_and_serializer[0]
        content = JSONRenderer().render(client_payment_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = ClientPaymentSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        client_payments = ClientPayment.objects.all()

        assert client_payments.count() == 2
        assert client_payments.last().client_id == client_payment.client_id
        assert client_payments.last().order_id == client_payment.order_id
        assert client_payments.last().created_at == client_payment.created_at


class TestShopperPaymentSerializer:
    @pytest.mark.django_db
    def test_shopper_payment_serialization(self, shopper_payment_and_serializer):
        shopper_payment = shopper_payment_and_serializer[0]
        expected_serializer_data = shopper_payment_and_serializer[1]
        serializer_data = ShopperPaymentSerializer(shopper_payment).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_shopper_payment_deserialization(self, shopper_payment_and_serializer):
        shopper_payment = shopper_payment_and_serializer[0]
        content = JSONRenderer().render(shopper_payment_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = ShopperPaymentSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        shopper_payments = ShopperPayment.objects.all()

        assert shopper_payments.count() == 2
        assert shopper_payments.last().shopper_id == shopper_payment.shopper_id
        assert shopper_payments.last().order_id == shopper_payment.order_id
        assert shopper_payments.last().created_at == shopper_payment.created_at
