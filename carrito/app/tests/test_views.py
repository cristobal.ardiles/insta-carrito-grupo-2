import pytest
from rest_framework import status
from model_bakery import baker
from app.serializers import (
    ClientSerializer,
    ShopperSerializer,
    DeliveryAddressSerializer,
    StoreSerializer,
    ProductSerializer,
    OrderSerializer,
    RouteSerializer,
    BackgroundCheckSerializer,
    ClientPaymentSerializer,
    ShopperPaymentSerializer,
)

# --- project ---


class TestClientListApiView:
    @pytest.mark.django_db
    def test_clients_get(self, client):
        client1 = baker.make_recipe("app.tests.example_client")
        client2 = baker.make_recipe(
            "app.tests.example_client",
            id=2,
        )
        client_list = []
        client_list.append(ClientSerializer(client1).data)
        client_list.append(ClientSerializer(client2).data)
        request = client.get("/api/clients/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == client_list


class TestClientDetailApiView:
    @pytest.mark.django_db
    def test_detail_client_get(self, client):
        insta_client = baker.make_recipe("app.tests.example_client")
        id = insta_client.id
        request = client.get(f"/api/clients/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == ClientSerializer(insta_client).data

    @pytest.mark.django_db
    def test_detail_no_client_get(self, client):
        request = client.get("/api/clients/1/")
        assert request.status_code == 404


class TestShopperListApiView:
    @pytest.mark.django_db
    def test_shopper_get(self, client):
        shopper1 = baker.make_recipe("app.tests.example_shopper")
        shopper2 = baker.make_recipe(
            "app.tests.example_shopper",
            id=2,
        )
        shopper_list = []
        shopper_list.append(ShopperSerializer(shopper1).data)
        shopper_list.append(ShopperSerializer(shopper2).data)
        request = client.get("/api/shoppers/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == shopper_list


class TestShopperDetailApiView:
    @pytest.mark.django_db
    def test_detail_shopper_get(self, client):
        insta_shopper = baker.make_recipe("app.tests.example_shopper")
        id = insta_shopper.id
        request = client.get(f"/api/shoppers/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == ShopperSerializer(insta_shopper).data

    @pytest.mark.django_db
    def test_detail_no_shopper_get(self, client):
        request = client.get("/api/shoppers/1/")
        assert request.status_code == 404


class TestRouteListApiView:
    @pytest.mark.django_db
    def test_routes_get(self, client):
        route1 = baker.make_recipe("app.tests.example_route")
        route2 = baker.make_recipe(
            "app.tests.example_route",
            id=2,
        )
        route_list = []
        route_list.append(RouteSerializer(route1).data)
        route_list.append(RouteSerializer(route2).data)
        request = client.get("/api/routes/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        print(response)
        assert response == route_list


class TestRouteDetailApiView:
    @pytest.mark.django_db
    def test_detail_route_get(self, client):
        route = baker.make_recipe("app.tests.example_route")
        id = route.id
        request = client.get(f"/api/routes/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == RouteSerializer(route).data

    @pytest.mark.django_db
    def test_detail_no_route_get(self, client):
        request = client.get("/api/routes/1/")
        assert request.status_code == 404


class TestBackgroundCheckListApiView:
    @pytest.mark.django_db
    def test_background_check_get(self, client):
        background_check1 = baker.make_recipe("app.tests.example_background_check")
        background_check2 = baker.make_recipe(
            "app.tests.example_background_check",
            id=2,
        )
        background_check_list = []
        background_check_list.append(
            BackgroundCheckSerializer(background_check1).data
        )
        background_check_list.append(
            BackgroundCheckSerializer(background_check2).data
        )
        request = client.get("/api/background-checks/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == background_check_list


class TestBackgroundCheckDetailApiView:
    @pytest.mark.django_db
    def test_detail_background_check_get(self, client):
        background_check = baker.make_recipe("app.tests.example_background_check")
        id = background_check.id
        request = client.get(f"/api/background-checks/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == BackgroundCheckSerializer(background_check).data

    @pytest.mark.django_db
    def test_detail_no_background_check_get(self, client):
        request = client.get("/api/background-checks/1/")
        assert request.status_code == 404


class TestClientPaymentListApiView:
    @pytest.mark.django_db
    def test_client_payment_get(self, client):
        client_payment1 = baker.make_recipe("app.tests.example_client_payment")
        client_payment2 = baker.make_recipe(
            "app.tests.example_client_payment",
            id=2,
        )
        client_payment_list = []
        client_payment_list.append(ClientPaymentSerializer(client_payment1).data)
        client_payment_list.append(ClientPaymentSerializer(client_payment2).data)
        request = client.get("/api/client-payments/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == client_payment_list


class TestClientPaymentDetailApiView:
    @pytest.mark.django_db
    def test_detail_client_payment_get(self, client):
        client_payment = baker.make_recipe("app.tests.example_client_payment")
        id = client_payment.id
        request = client.get(f"/api/client-payments/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == ClientPaymentSerializer(client_payment).data

    @pytest.mark.django_db
    def test_detail_no_client_payment_get(self, client):
        request = client.get("/api/client-payments/1/")
        assert request.status_code == 404


class TestShopperPaymentListApiView:
    @pytest.mark.django_db
    def test_shopper_payment_get(self, client):
        shopper_payment1 = baker.make_recipe("app.tests.example_shopper_payment")
        shopper_payment2 = baker.make_recipe(
            "app.tests.example_shopper_payment",
            id=2,
        )
        shopper_payment_list = []
        shopper_payment_list.append(
            ShopperPaymentSerializer(shopper_payment1).data
        )
        shopper_payment_list.append(
            ShopperPaymentSerializer(shopper_payment2).data
        )
        request = client.get("/api/shopper-payments/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == shopper_payment_list


class TestShopperPaymentDetailApiView:
    @pytest.mark.django_db
    def test_detail_shopper_payment_get(self, client):
        shopper_payment = baker.make_recipe("app.tests.example_shopper_payment")
        id = shopper_payment.id
        request = client.get(f"/api/shopper-payments/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == ShopperPaymentSerializer(shopper_payment).data

    @pytest.mark.django_db
    def test_detail_no_shopper_payment_get(self, client):
        request = client.get("/api/shopper-payments/1/")
        assert request.status_code == 404


class TestDeliveryAddressListApiView:
    @pytest.mark.django_db
    def test_delivery_address_get(self, client):
        delivery_address1 = baker.make_recipe("app.tests.example_delivery_address")
        delivery_address2 = baker.make_recipe(
            "app.tests.example_delivery_address",
            id=2,
        )
        delivery_address_list = []
        delivery_address_list.append(
            DeliveryAddressSerializer(delivery_address1).data
        )
        delivery_address_list.append(
            DeliveryAddressSerializer(delivery_address2).data
        )
        request = client.get("/api/delivery-addresses/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == delivery_address_list


class TestDeliveryAddressDetailApiView:
    @pytest.mark.django_db
    def test_detail_delivery_address_get(self, client):
        delivery_address = baker.make_recipe("app.tests.example_delivery_address")
        id = delivery_address.id
        request = client.get(f"/api/delivery-addresses/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == DeliveryAddressSerializer(delivery_address).data

    @pytest.mark.django_db
    def test_detail_no_delivery_address_get(self, client):
        request = client.get("/api/delivery_addresses/1/")
        assert request.status_code == 404


class TestStoreListApiView:
    @pytest.mark.django_db
    def test_store_get(self, client):
        store1 = baker.make_recipe("app.tests.example_store")
        store2 = baker.make_recipe(
            "app.tests.example_store",
            id=2,
        )
        store_list = []
        store_list.append(StoreSerializer(store1).data)
        store_list.append(StoreSerializer(store2).data)
        request = client.get("/api/stores/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == store_list


class TestStoreDetailApiView:
    @pytest.mark.django_db
    def test_detail_store_get(self, client):
        store = baker.make_recipe("app.tests.example_store")
        id = store.id
        request = client.get(f"/api/stores/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == StoreSerializer(store).data

    @pytest.mark.django_db
    def test_detail_no_store_get(self, client):
        request = client.get("/api/stores/1/")
        assert request.status_code == 404


class TestProductListApiView:
    @pytest.mark.django_db
    def test_product_get(self, client):
        product1 = baker.make_recipe("app.tests.example_product1")
        product2 = baker.make_recipe(
            "app.tests.example_product2",
            id=2,
        )
        product_list = []
        product_list.append(ProductSerializer(product1).data)
        product_list.append(ProductSerializer(product2).data)
        request = client.get("/api/products/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == product_list


class TestProductDetailApiView:
    @pytest.mark.django_db
    def test_detail_product_get(self, client):
        product = baker.make_recipe("app.tests.example_product1")
        id = product.id
        request = client.get(f"/api/products/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == ProductSerializer(product).data

    @pytest.mark.django_db
    def test_detail_no_product_get(self, client):
        request = client.get("/api/products/1/")
        assert request.status_code == 404


class TestOrderListApiView:
    @pytest.mark.django_db
    def test_order_get(self, client):
        order1 = baker.make_recipe("app.tests.example_order")
        order2 = baker.make_recipe(
            "app.tests.example_order",
            id=2,
        )
        order_list = []
        order_list.append(OrderSerializer(order1).data)
        order_list.append(OrderSerializer(order2).data)
        request = client.get("/api/orders/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == order_list


class TestOrderDetailApiView:
    @pytest.mark.django_db
    def test_detail_order_get(self, client):
        order = baker.make_recipe("app.tests.example_order")
        id = order.id
        request = client.get(f"/api/orders/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == OrderSerializer(order).data

    @pytest.mark.django_db
    def test_detail_no_order_get(self, client):
        request = client.get("/api/orders/1/")
        assert request.status_code == 404
