from .models import (
    Client,
    Shopper,
    Route,
    Order,
    Store,
    DeliveryAddress,
    Product,
    BackgroundCheck,
    ClientPayment,
    ShopperPayment,
)

from .serializers import (
    ClientSerializer,
    ShopperSerializer,
    RouteSerializer,
    OrderSerializer,
    StoreSerializer,
    ProductSerializer,
    DeliveryAddressSerializer,
    BackgroundCheckSerializer,
    ClientPaymentSerializer,
    ShopperPaymentSerializer,
)

from rest_framework import mixins
from rest_framework import generics

# Create your views here.

# import random
# from rest_framework.decorators import api_view

# --- examples ---

# @api_view(["GET", "POST"])
# def index(request):
#     """
#     Displays a random integer between 0 and 100, or returns the div of a random
#     number between 0 and 100 by a given integer.
#     """
#     if request.method == "GET":
#         random_integer = random.randrange(101)
#         dict = {"random integer value": random_integer}
#         return Response(dict)

#     elif request.method == "POST":
#         input_integer = int(request.data["value"])
#         random_integer = random.randrange(101)
#         if input_integer == 0:
#             dict = {
#                 "random integer to divide": random_integer,
#                 "input integer": input_integer,
#                 "Error": "cannot divide a number by zero.",
#             }
#             return Response(dict, status=status.HTTP_400_BAD_REQUEST)
#         div = random_integer // input_integer
#         dict = {
#             "random integer to divide": random_integer,
#             "input integer": input_integer,
#             "floor division value": div,
#         }
#         return Response(dict, status=status.HTTP_200_OK)

# --- project ---

# Client views


class ClientListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ClientDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Shopper views


class ShopperListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Shopper.objects.all()
    serializer_class = ShopperSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ShopperDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Shopper.objects.all()
    serializer_class = ShopperSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Delivery Address views


class DeliveryAddressListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = DeliveryAddress.objects.all()
    serializer_class = DeliveryAddressSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class DeliveryAddressDetailApiView(
    mixins.RetrieveModelMixin, generics.GenericAPIView
):
    queryset = DeliveryAddress.objects.all()
    serializer_class = DeliveryAddressSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Store views


class StoreListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class StoreDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Product views


class ProductListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ProductDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Order views


class OrderListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class OrderDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Route views


class RouteListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Route.objects.all()
    serializer_class = RouteSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RouteDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Route.objects.all()
    serializer_class = RouteSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Background Check views


class BackgroundCheckListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = BackgroundCheck.objects.all()
    serializer_class = BackgroundCheckSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class BackgroundCheckDetailApiView(
    mixins.RetrieveModelMixin, generics.GenericAPIView
):
    queryset = BackgroundCheck.objects.all()
    serializer_class = BackgroundCheckSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Client Payment views


class ClientPaymentListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = ClientPayment.objects.all()
    serializer_class = ClientPaymentSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ClientPaymentDetailApiView(
    mixins.RetrieveModelMixin, generics.GenericAPIView
):
    queryset = ClientPayment.objects.all()
    serializer_class = ClientPaymentSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Shopper Payment views


class ShopperPaymentListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = ShopperPayment.objects.all()
    serializer_class = ShopperPaymentSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ShopperPaymentDetailApiView(
    mixins.RetrieveModelMixin, generics.GenericAPIView
):
    queryset = ShopperPayment.objects.all()
    serializer_class = ShopperPaymentSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
